
class MonitTypes(object):
    event_type = {
        "0"         : "Event_Null",   
        "1"         : "Event_Checksum",
        "2"         : "Event_Resource",
        "4"         : "Event_Timeout",
        "8"         : "Event_Timestamp",
        "16"        : "Event_Size",
        "32"        : "Event_Connection",
        "64"        : "Event_Permission",
        "128"       : "Event_Uid",
        "256"       : "Event_Gid",
        "512"       : "Event_Nonexist",
        "1024"      : "Event_Invalid",
        "2048"      : "Event_Data",
        "4096"      : "Event_Exec",
        "8192"      : "Event_Fsflag",
        "16384"     : "Event_Icmp",
        "32768"     : "Event_Content",
        "65536"     : "Event_Instance",
        "131072"    : "Event_Action",
        "262144"    : "Event_Pid",
        "524288"    : "Event_PPid",
        "1048576"   : "Event_Heartbeat",
        "2097152"   : "Event_Status",
        "4194304"   : "Event_Uptime",
        "8388608"   : "Event_Link",
        "16777216"  : "Event_Speed",
        "33554432"  : "Event_Saturation",
        "67108864"  : "Event_ByteIn",
        "134217728" : "Event_ByteOut",
        "268435456" : "Event_PacketIn",
        "536870912" : "Event_PacketOut",
        "1073741824": "Event_Exists",
        "2147483647": "Event_All"
    }

    event_type_mapping = {
            "Event_Action": [
                "Action done",
                "Action done",
                "Action done",
                "Action done"
            ],
            "Event_ByteIn": [
                "Download bytes exceeded",
                "Download bytes ok",
                "Download bytes changed",
                "Download bytes not changed"
            ],
            "Event_ByteOut": [
                "Upload bytes exceeded",
                "Upload bytes ok",
                "Upload bytes changed",
                "Upload bytes not changed"
            ],
            "Event_Checksum": [
                "Checksum failed",
                "Checksum succeeded",
                "Checksum changed",
                "Checksum not changed"
            ],
            "Event_Connection": [
                "Connection failed",
                "Connection succeeded",
                "Connection changed",
                "Connection not changed"
            ],
            "Event_Content": [
                "Content failed",
                "Content succeeded",
                "Content match",
                "Content doesn't match"
            ],
            "Event_Data": [
                "Data access error",
                "Data access succeeded",
                "Data access changed",
                "Data access not changed"
            ],
            "Event_Exec": [
                "Execution failed",
                "Execution succeeded",
                "Execution changed",
                "Execution not changed"
            ],
            "Event_Fsflag": [
                "Filesystem flags failed",
                "Filesystem flags succeeded",
                "Filesystem flags changed",
                "Filesystem flags not changed"
            ],
            "Event_Gid": [
                "GID failed",
                "GID succeeded",
                "GID changed",
                "GID not changed"
            ],
            "Event_Heartbeat": [
                "Heartbeat failed",
                "Heartbeat succeeded",
                "Heartbeat changed",
                "Heartbeat not changed"
            ],
            "Event_Icmp": [
                "ICMP failed",
                "ICMP succeeded",
                "ICMP changed",
                "ICMP not changed"
            ],
            "Event_Instance": [
                "Monit instance failed",
                "Monit instance succeeded",
                "Monit instance changed",
                "Monit instance not changed"
            ],
            "Event_Invalid": [
                "Invalid type",
                "Type succeeded",
                "Type changed",
                "Type not changed"
            ],
            "Event_Link": [
                "Link down",
                "Link up",
                "Link changed",
                "Link not changed"
            ],
            "Event_Nonexist": [
                "Does not exist",
                "Exists",
                "Existence changed",
                "Existence not changed"
            ],
            "Event_PacketIn": [
                "Download packets exceeded",
                "Download packets ok",
                "Download packets changed",
                "Download packets not changed"
            ],
            "Event_PacketOut": [
                "Upload packets exceeded",
                "Upload packets ok",
                "Upload packets changed",
                "Upload packets not changed"
            ],
            "Event_Permission": [
                "Permission failed",
                "Permission succeeded",
                "Permission changed",
                "Permission not changed"
            ],
            "Event_Pid": [
                "PID failed",
                "PID succeeded",
                "PID changed",
                "PID not changed"
            ],
            "Event_PPid": [
                "PPID failed",
                "PPID succeeded",
                "PPID changed",
                "PPID not changed"
            ],
            "Event_Resource": [
                "Resource limit matched",
                "Resource limit succeeded",
                "Resource limit changed",
                "Resource limit not changed"
            ],
            "Event_Saturation": [
                "Saturation exceeded",
                "Saturation ok",
                "Saturation changed",
                "Saturation not changed"
            ],
            "Event_Size": [
                "Size failed",
                "Size succeeded",
                "Size changed",
                "Size not changed"
            ],
            "Event_Speed": [
                "Speed failed",
                "Speed ok",
                "Speed changed",
                "Speed not changed"
            ],
            "Event_Status": [
                "Status failed",
                "Status succeeded",
                "Status changed",
                "Status not changed"
            ],
            "Event_Timeout": [
                "Timeout",
                "Timeout recovery",
                "Timeout changed",
                "Timeout not changed"
            ],
            "Event_Timestamp": [
                "Timestamp failed",
                "Timestamp succeeded",
                "Timestamp changed",
                "Timestamp not changed"
            ],
            "Event_Uid": [
                "UID failed",
                "UID succeeded",
                "UID changed",
                "UID not changed"
            ],
            "Event_Uptime": [
                "Uptime failed",
                "Uptime succeeded",
                "Uptime changed",
                "Uptime not changed"
            ],
            "Event_Null": [
                "No Event",
                "No Event",
                "No Event",
                "No Event"
            ]
    }

    monitor_state = {
            "0": { "monit_value": "Monitor_Not", "value": "not_monitored" },
            "1": { "monit_value": "Monitor_Yes", "value": "monitored" },
            "2": { "monit_value": "Monitor_Init", "value": "initializating" },
            "4": { "monit_value": "Monitor_Waiting", "value": "waiting" }
    }

    monitor_mode = {
            "0": { "monit_value": "Monitor_Active", "value": "active" },
            "1": { "monit_value": "Monitor_Passive", "value": "passive" }
    }

    onreboot_type = {
            "0" : { "monit_value" : "Onreboot_Start", "value" : "start"},
            "1" : { "monit_value" : "Onreboot_Nostart", "value" : "nostart"},
            "2" : { "monit_value" : "Onreboot_Laststate", "value" : "laststate"}
    }


    every_type = {
        "0" : { "monit_value" : "Every_Cycle", "value" : "every_cycle" },
        "1" : { "monit_value" : "Every_SkipCycles", "value" : "every_skipped_cycles" },
        "2" : { "monit_value" : "Every_Cron", "value" : "every_cron" },
        "3" : { "monit_value" : "Every_NotInCron", "value" : "every_not_in_cron" }
    }

    service_type = {
            "0": { "monit_value": "Service_Filesystem", "value": "filesystem" },
            "1": { "monit_value": "Service_Directory", "value": "directory" },
            "2": { "monit_value": "Service_File", "value": "file" },
            "3": { "monit_value": "Service_Process", "value": "process" },
            "4": { "monit_value": "Service_Host", "value": "host" },
            "5": { "monit_value": "Service_System", "value": "system" },
            "6": { "monit_value": "Service_Fifo", "value": "fifo" },
            "7": { "monit_value": "Service_Program", "value": "program" },
            "8": { "monit_value": "Service_Net", "value": "net" }
    }

    action_type = {
            "0": { "monit_value": "Action_Ignored", "value": "ignore" },
            "1": { "monit_value": "Action_Alert", "value": "alert" },
            "2": { "monit_value": "Action_Restart", "value": "restart" },
            "3": { "monit_value": "Action_Stop", "value": "stop" },
            "4": { "monit_value": "Action_Exec", "value": "execute" },
            "5": { "monit_value": "Action_Unmonitor", "value": "unmonitor" },
            "6": { "monit_value": "Action_Start", "value": "start" },
            "7": { "monit_value": "Action_Monitor", "value": "monitor" }
    }
