#!/usr/bin/env python3

import xmltodict
import json
from utils import MonitTypes as mt
import requests
import graphyte
from flask import Flask
from flask import jsonify
from flask import request 

app = Flask('dmonit')

def translate_xml_to_json(xmld, version=0):
    doc = json.loads(json.dumps(xmltodict.parse(xmld, attr_prefix=''), indent=4))
    if version == 0:
        _ms = doc['monit']['service']
    else:
        _ms = doc['monit']['services']['service']
        try:
            del(doc['monit']['services'])
            del(doc['monit']['servicegroups'])
        except:
            pass
    if not isinstance(_ms, list):
        _ms = [ _ms ]
    for s in _ms:
        s['type'] = mt.service_type[s['type']]['value']
        #s['status'] = mt.event_type_mapping[mt.event_type[s['status_hint']]][int(s['status'])].lower()
        s['monitor'] = mt.monitor_state[s['monitor']]['value']
        s['monitormode'] = mt.monitor_mode[s['monitormode']]['value']
        s['onreboot'] = mt.onreboot_type[s['onreboot']]['value']
        s['pendingaction'] = mt.action_type[s['pendingaction']]['value']
        if 'every' in s.keys():
            if 'type' in s['every'].keys():
                s['every']['type'] = mt.every_type[s['every']['type']]['value']
    
    doc['monit']['service'] = _ms
    return doc

def dict_to_hierhical_string(data, key, timestamp):
    _db = []
    for k in data.keys():
        if type(data[k]) == dict:
            _db.extend(dict_to_hierhical_string(data[k], '{}.{}'.format(key, k), timestamp))
        elif type(data[k]) == list:
                for i in range(0, len(data[k])):
                    if k == 'service':
                        _db.extend(dict_to_hierhical_string(data[k][i], '{}.{}.{}.{}'.format(key, k, data[k][i]['type'], data[k][i]['name'].replace('.', '_')), timestamp))
                    else:
                        _db.extend(dict_to_hierhical_string(data[k][i], '{}.{}.v{}'.format(key, k, i), timestamp))
        else:
            if data[k] == None:
                data[k] = '0'
            if k == 'collected_sec':
                continue
            if k == 'collected_usec':
                continue
            if k == 'status_hint':
                continue
            if k == 'name':
                continue
            if k == 'type':
                continue
            try:
                float(data[k])
            except ValueError:
                continue
            _db.append(('{}.{}'.format(key, k), data[k].replace(" ", "_").replace(',', '_'), timestamp))
    return _db

def transform_to_graphite(doc):
    _sid = doc['server']['localhostname']
    _timestamp = doc['service'][0]['collected_sec']
    _new_sid = _sid.split('.')
    if len(_new_sid) > 1:
        _new_sid = '.'.join(_new_sid[::-1])
    else:
        _new_sid = _new_sid[0]
    _new_sid += '.monit'
    return dict_to_hierhical_string(doc, _new_sid, _timestamp)

def push_to_graphite(payload):
    graphyte.init('localhost')
    for e in payload:
        print('Pushing: {} {} {}'.format(*e))
        graphyte.send(e[0], float(e[1]), timestamp=float(e[2]))

@app.route('/', methods=['GET'])
def dmIndex():
    return jsonify({'ok':'True', 'message': "DMonit -- Monit unleashed", 'payload': ''})

@app.route('/collector', methods=['POST'])
def dmCollector():
    _data = request.get_data().decode()
    _doc = translate_xml_to_json(_data, version=1)
    push_to_graphite(transform_to_graphite(_doc['monit']))
    return jsonify({'ok':'True', 'message': "DMonit -- Monit Collector unleashed", 'payload': ''})

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
    #_xml = requests.get('http://monit.instance/_status?format=xml', auth=('admin', 'password'))
    #_doc = translate_xml_to_json(_xml.text)
    #print(json.dumps(_doc, indent=4))
    #push_to_graphite(transform_to_graphite(_doc['monit']))
    #for e in transform_to_graphite(_doc['monit']):
    #    print(e[0], e[1], e[2])